package ictgradschool.industry.lab17.ex04;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestShapeAreaCalculator {
    private ShapeAreaCalculator myshapeCalculator;

    @Before
    public void setUp(){
        myshapeCalculator = new ShapeAreaCalculator();
    }

    @Test
    public void testConvertStringToDouble(){
        double converted = myshapeCalculator.convertStringToDouble("3.14");
        assertEquals(3.14, converted, 1e-15);
    }

    @Test
    public void testCalculateRectangleArea(){
        double w = 2.5;
        double l = 1.2;
        double area = myshapeCalculator.calculateRectangleArea(w, l);
        assertEquals(3.0, area, 1e-15);
    }

    @Test
    public void testCalculateCircleArea(){
        double r = 6.4;
        double area = myshapeCalculator.calculateCircleArea(r);
        assertEquals(128.6796350910379310474, area, 1e-15);
    }

    @Test
    public void testRoundAreaToInt(){
        double a = 2.17282;
        int rounded = myshapeCalculator.roundAreaToInt(a);
        assertEquals(2, rounded);
    }

    @Test
    public void testSmallerArea(){
        double a1 = 8.76;
        double a2 = 6.78;
        assertEquals(a2, myshapeCalculator.smallerArea(a1, a2), 1e-15);
    }
}
