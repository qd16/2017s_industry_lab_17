package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }


    private void turnTimesDirection(int turnTimes) {
        switch (turnTimes%4){
            case 0:
                assertEquals(Robot.Direction.North, myRobot.getDirection());
                break;
            case 1:
                assertEquals(Robot.Direction.East, myRobot.getDirection());
                break;
            case 2:
                assertEquals(Robot.Direction.South, myRobot.getDirection());
                break;
            case 3:
                assertEquals(Robot.Direction.West, myRobot.getDirection());
                break;
        }
    }

    @Test
    public void testTurnsOne(){
        int turnTimes = 1;
        for (int i = 0; i < turnTimes; i++) {
            myRobot.turn();
        }

        turnTimesDirection(turnTimes);
    }

    @Test
    public void testTurnsTwo(){
        int turnTimes = 2;
        for (int i = 0; i < turnTimes; i++) {
            myRobot.turn();
        }

        turnTimesDirection(turnTimes);
    }

    @Test
    public void testTurnsThree(){
        int turnTimes = 3;
        for (int i = 0; i < turnTimes; i++) {
            myRobot.turn();
        }

        turnTimesDirection(turnTimes);
    }

    @Test
    public void testTurnsFour(){
        int turnTimes = 4;
        for (int i = 0; i < turnTimes; i++) {
            myRobot.turn();
        }

        turnTimesDirection(turnTimes);
    }

    @Test
    public void testMoveNorth() {

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(9, myRobot.currentState().row);
        assertEquals(1, myRobot.currentState().column);
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testMoveEast() {

        try {
            myRobot.turn();
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(10, myRobot.currentState().row);
        assertEquals(2, myRobot.currentState().column);
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;
        try {
            myRobot.turn();
            // Move the robot to the right row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }

    @Test
    public void testMoveSouth() {

        try {
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(10, myRobot.currentState().row);
        assertEquals(1, myRobot.currentState().column);
    }

    @Test
    public void testIllegalMoveSouth() {
        boolean atBottom = false;
        try {
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            // Move the robot to the right row.
            //for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atBottom = myRobot.currentState().row == 10;
            assertTrue(atBottom);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testMoveWest() {

        try {
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(10, myRobot.currentState().row);
        assertEquals(1, myRobot.currentState().column);
    }

    @Test
    public void testIllegalMoveWest() {
        boolean atLeft = false;
        try {
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            // Move the robot to the right row.
            //for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
            myRobot.move();

            // Check that robot has reached the top.
            atLeft = myRobot.currentState().column == 1;
            assertTrue(atLeft);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }
}
