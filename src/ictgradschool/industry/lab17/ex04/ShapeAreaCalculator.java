package ictgradschool.industry.lab17.ex04;

public class ShapeAreaCalculator {
    public double convertStringToDouble(String s) {
        return Double.parseDouble(s);
    }

    public double calculateRectangleArea(double w, double l) {
        return w*l;
    }

    public double calculateCircleArea(double r) {
        return Math.PI*r*r;
    }

    public int roundAreaToInt(double a) {
        return (int)Math.round(a);
    }

    public double smallerArea(double a1, double a2) {
        return Math.min(a1, a2);
    }
}
